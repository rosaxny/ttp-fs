'use strict';
const { Portfolio } = require('./portfolio-model');

const axios = require('axios');
const passport = require('passport');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const express = require('express');
const router = express.Router();

const jwtAuth = passport.authenticate('jwt', { session: false });

router.use(express.json());

// when user buys new stock - `transactions` is updated
router.put('/transaction', jwtAuth, async (req, res) => {
    console.log('PUT request `/transaction`', req.body) // username, stock, shares, balance
    const requiredFields = ['username', 'stock', 'shares', 'balance'];
    const missingField = requiredFields.find((field) => !(field in req.body));
    const { username, stock, balance } = req.body;
    let shares = Math.floor(req.body.shares);
    if (missingField) {
        return res
            .status(422)
            .json({
                code: 422,
                reason: 'ValidationError',
                message: `Missing ${missingField} in request body`,
                location: missingField
            })
    }

    if (typeof username !== 'string' || username === '' || username.trim().length === 0) {
        return res
            .status(422)
            .json({
                code: 422,
                reason: 'ValidationError',
                message: `Username must be a non-empty string with no leading or trailing spaces`,
                location: 'username'
            })
    }

    let data;
    let totalPrice = 0;

    // make API request to check price of stock
    // invalid stock/ticker symbol will return an empty array
    // https://api.iextrading.com/1.0/tops?symbols=SNAP
    let resAxios = await axios({
        method: 'get',
        url: `https://api.iextrading.com/1.0/tops?symbols=${stock}`
    })
    .catch((err) => {
        if (err.isAxiosError === true) {
            return res.status(400).json({ code: 400, error: 'an error occurred' })
        }
    });

    data = resAxios.data;

    if (data.length === 0) {
        // return error message to check the ticker symbol
        return res.status(400).json({ code: 400, error: 'an error occurred' })
    };
    
    // if askprice is 0, return error
    if (data[0].askPrice === 0) {
        return res.status(400).json({ code: 400, error: 'an error occurred' })
    };

    // otherwise, calculate totalPrice
    // get askPrice
    // multiply by number of shares the user wants to purchase
    totalPrice = data[0].askPrice * shares;

    // compare totalPrice to user's current balance
    // not enough funds, return error message
    if (balance < totalPrice) {
        return res.status(500).json({ code: 500, error: 'insufficient funds' })
    };

    // enough funds to make purchase
    // update balance & transactions
    // add object { stock: "", shares: 0, purchaseDate: "", totalPrice } to transactions array
    const newBalance = balance - totalPrice;
    
    const newTransaction = {
        created: Date(),
        stock,
        shares,
        price: data[0].askPrice,
        totalPrice
    }
    
    const updateBalance = {
        balance: newBalance
    }

    return Portfolio
        .findOneAndUpdate({ username }, { $set: updateBalance, $push: { transactions: newTransaction }}, { new: true })
        .then((updatedPortfolio) => {
            console.log('portfolio has been updated', updatedPortfolio);
            return res.json({ message: 'portfolio has been updated', updatedPortfolio });
        })
        .catch((err) => {
            console.error('error updating portfolio', err);
            if (err.reason === 'ValidationError') {
                return res.status(err.code).json(err);
            }
            return res.status(500).json({ code: 500, error: 'internal server error' });
        })
})

router.get('/', jwtAuth, (req, res) => {
    console.log('GET `/portfolio` with req', req) // keys are username and password
    const { username } = req.user
    // if (typeof username !== 'string' || username === '' || username.trim().length === 0) {
    //     return res
    //         .status(422)
    //         .json({
    //             code: 422,
    //             reason: 'ValidationError',
    //             message: `Username must be a non-empty string with no leading or trailing spaces`,
    //             location: 'username'
    //         })
    // }
    return Portfolio
        .findOne({ username })
        .then(async (portfolio) => {
            console.log(portfolio, 'portfolio')
            let transactions = portfolio.transactions;
            if (transactions.length === 0) {
                return res.json(portfolio)
            }

            // loop through transactions and count shares per stock
            let countStockShares = {}
            for (let i = 0; i < transactions.length; i++) {
                let tickerSymbol = transactions[i].stock;
                let sharesPurchased = transactions[i].shares;
                let totalPrice = transactions[i].totalPrice;

                if (!countStockShares[tickerSymbol]) {
                    countStockShares[tickerSymbol] = {
                        shares: sharesPurchased,
                        purchasePrice: 0
                    };
                } else {
                    countStockShares[tickerSymbol].shares += sharesPurchased;
                }
                countStockShares[tickerSymbol].purchasePrice += totalPrice;
            }

            // calculate average purchase price
            for (let i = 0; i < Object.keys(countStockShares).length; i++) {
                let tickerSymbol = Object.keys(countStockShares)[i]
                countStockShares[tickerSymbol].avg = countStockShares[tickerSymbol].purchasePrice / countStockShares[tickerSymbol].shares;
            }
            
            // get current price
            for (let i = 0; i < Object.keys(countStockShares).length; i++) {
                let tickerSymbol = Object.keys(countStockShares)[i];
                await axios({
                    method: 'get',
                    url: `https://api.iextrading.com/1.0/tops?symbols=${tickerSymbol}`
                })
                .then((res) => {
                    let currentPrice = res.data[0].lastSalePrice;
                    countStockShares[tickerSymbol].currentPrice = currentPrice;
                    return countStockShares
                })
                .catch((err) => {
                    if (err.isAxiosError === true) {
                        return res.json({ error: 'an error occurred' })
                    }
                })
            }

            let resultPortfolio = {
                countStockShares,
                balance: parseFloat(portfolio.balance.toFixed(2))
            }
            
            return res.json(resultPortfolio);

            // GET OFFICIAL PRICE ENDPOINT
            // currently returns empty object

            // for (let i = 0; i < Object.keys(countStockShares).length; i++) {
            //     let tickerSymbol = Object.keys(countStockShares)[i];
            //     let currentPrice = axios({
            //         method: 'get',
            //         url: `https://api.iextrading.com/1.0/deep/official-price?symbols=${tickerSymbol}`
            //     })
            //     .then((res) => {
            //         let officialPrice = res.data[0].price
            //         countStockShares[tickerSymbol].currentPrice = officialPrice;
            //     })
            //     .catch((err) => {
            //         if (err.isAxiosError === true) {
            //             return res.json({ error: 'an error occurred' })
            //         }
            //     })
            // }
        })
        .catch((err) => {
            console.error('error getting portfolio', err);
            return res.status(500).json({ message: 'internal server error' });
        })
})

router.get('/transactions', (req, res) => {
    return Portfolio
        .findOne({ username: req.user.username })
        .then((portfolio) => {
            return res.json(portfolio)
        })
        .catch((err) => {
            console.error('error getting transactions', err);
            return res.status(500).json({ message: 'internal server error' });
        })
})

module.exports = { router };