'use strict';
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const transactionsSchema = new mongoose.Schema({
    created: Date,
    stock: {
        type: String,
        required: true
    },
    shares: {
        type: Number,
        required: true,
        default: 1
    },
    price: {
        type: Number,
        required: true
    },
    totalPrice: {
        type: Number,
        required: true
    }
});

const PortfolioSchema = mongoose.Schema({
    username: {
        type: String
    },
    name: {
        type: String
    },
    balance: {
        type: Number,
        required: true,
        default: 5000
    },
    transactions: [transactionsSchema]
});



const Portfolio = mongoose.model('Portfolio', PortfolioSchema);

module.exports = { Portfolio };