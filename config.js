'use strict';
require('dotenv').config();
module.exports = {
    DATABASE_URL: process.env.DATABASE_URL || 'mongodb://localhost/ttp-fs',
    JWT_SECRET: process.env.JWT_SECRET,
    JWT_EXPIRY: process.env.JWT_EXPIRY || '7d',
    TEST_DATABASE_URL: process.env.TEST_DATABASE_URL || 'mongodb://localhost/test-ttp-fs',
    PORT: process.env.PORT || 8080
}