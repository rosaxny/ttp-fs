'use strict';
const { TEST_DATABASE_URL, JWT_SECRET } = require('../config');
const { app, runServer, closeServer } = require('../server');
const jwt = require("jsonwebtoken");

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const chai = require('chai');
const chaiHttp = require('chai-http');

const expect = chai.expect;
chai.use(chaiHttp);

function tearDownDb() {
	return new Promise((resolve, reject) => {
		mongoose
			.connection
			.dropDatabase()
			.then((result) => resolve(result))
			.catch((err) => reject(err));
	});
}

describe('users API', function() {
	const testUser = {
		username: 'username',
		password: 'password'
	}

	before(() => {
		return runServer(TEST_DATABASE_URL);
	})
	afterEach(() => {
		return tearDownDb();
	})
	after(() => {
		return closeServer();
	})

	describe('POST /api/users/register', () => {
		it('should reject submissions with missing usernames', () => {
			let user = {
				password: 'password'
			}
			return chai
				.request(app)
				.post('/api/users/register')
				.send(user)
				.then((res) => {
					expect(res).to.be.json;
					expect(res).to.have.status(422);
					expect(res.body).to.be.a('object');
					expect(res.body.code).to.be.equal(422);
					expect(res.body.reason).to.be.equal('ValidationError');
				})
		})

		it('should reject submissions with array usernames', () => {
			let user = {
				username: [],
				password: 'password'
			}
			return chai
				.request(app)
				.post('/api/users/register')
				.send(user)
				.then((res) => {
					expect(res).to.be.json;
					expect(res).to.have.status(422);
					expect(res.body).to.be.a('object');
					expect(res.body.code).to.be.equal(422);
					expect(res.body.reason).to.be.equal('ValidationError');
					expect(res.body.location).to.be.equal('username');
				})
		})

		it('should reject submissions with object usernames', () => {
			let user = {
				username: {},
				password: 'password'
			}
			return chai
				.request(app)
				.post('/api/users/register')
				.send(user)
				.then((res) => {
					expect(res).to.be.json;
					expect(res).to.have.status(422);
					expect(res.body).to.be.a('object');
					expect(res.body.code).to.be.equal(422);
					expect(res.body.reason).to.be.equal('ValidationError');
					expect(res.body.location).to.be.equal('username');
				})
		})

		it('should reject submissions with empty usernames', () => {
			let user = {
				username: '',
				password: 'password'
			}
			return chai
				.request(app)
				.post('/api/users/register')
				.send(user)
				.then((res) => {
					expect(res).to.be.json;
					expect(res).to.have.status(422);
					expect(res.body).to.be.a('object');
					expect(res.body.code).to.be.equal(422);
					expect(res.body.reason).to.be.equal('ValidationError');
					expect(res.body.location).to.be.equal('username');
				})
		})
	});
})