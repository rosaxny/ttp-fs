'use strict';
const { TEST_DATABASE_URL } = require('../config');
const { app, runServer, closeServer } = require('../server');
const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
chai.use(chaiHttp);

describe('static files', function() {
	before(function() {
		return runServer(TEST_DATABASE_URL);
	})
	after(function() {
		return closeServer();
	})
	describe('index page', function() {
		it('should exist', function() {
			return chai
				.request(app)
				.get('/')
				.then((res) => {
					expect(res).to.have.status(200);
				});
		})
	})
})