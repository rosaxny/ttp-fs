'use strict';
const { User } = require('./user-model');
const { Portfolio } = require('../portfolio/portfolio-model');
const { JWT_SECRET } = require('../config');

const jwt = require('jsonwebtoken');

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const express = require('express');
const router = express.Router();

router.use(express.json());

// create new user account
router.post('/register', (req, res) => {
    const { username, name, password } = req.body;
    console.log(username, 'post')
    // if (typeof username !== 'string' || username === '' || username.trim().length === 0) {
    //     return res
    //         .status(422)
    //         .json({
    //             code: 422,
    //             reason: 'ValidationError',
    //             message: `Username must be a non-empty string with no leading or trailing spaces`,
    //             location: 'username'
    //         })
    // }
    return User
        .findOne({ username })
        .then((_user) => {
            // if an account with that email already exists, return error
            if (_user) {
                return Promise.reject({
                    code: 422,
                    reason: 'ValidationError',
                    message: 'Email already registered',
                    location: 'email'
                })
            };

            // otherwise, hash the password
            return User.hashPassword(password);
        })
        .then((hash) => {
            return User.create({
                username,
                name,
                password: hash
            });
        })
        .then((user) => {
            return Portfolio
                .create({
                    username: user.username,
                    balance: 5000,
                    name: user.name,
                    transactions: []
                })
        })
        .then((newUser) => {
            console.log(newUser, 'newUser')
            // keys name, id, username, password - leave out password
            function createToken(user) {
                return jwt.sign({newUser}, JWT_SECRET, {
                    subject: user.username,
                    expiresIn: '7d',
                    algorithm: 'HS256'
                });
            }

            let token = createToken(newUser);
            return res.status(201).json({ token, username })
        })
        .catch((err) => {
            console.log(err)
            if (err.reason === 'ValidationError') {
                return res.status(err.code).json(err);
            }
            return res.status(500).json({
                code: 500,
                message: 'Internal server error'
            })
        })
})

module.exports = { router };