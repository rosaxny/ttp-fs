# TTP-FS

## API Documentation

GET `/api/portfolio`

```
{
    "countStockShares": {
        "snap": {
            "shares": 3,
            "purchasePrice": 45.77,
            "avg": 15.256666666666668,
            "currentPrice": 15.1
        },
        "fb": {
            "shares": 5,
            "purchasePrice": 1008.76,
            "avg": 201.752,
            "currentPrice": 201.18
        }
    },
    "balance": 4159.64
}
```

## Built With

| **Frontend** | **Backend** | **Testing and Deployment** |
| ------------- | ------------- | ------------- |
| HTML5  | NodeJS  | Mocha  |
| CSS3  | Express  | Chai (Chai HTTP)  |
| JavaScript (ES5 & ES6)  | dotenv  |  |
| | morgan  |  |
| | MongoDB  |  |
| | Mongoose  |  |
| | Passport (passport-jwt, passport-local) |  |
| | bcryptjs  |  |
| | jsonwebtoken  |  |
| |  |  |

## APIs Used
[IEX Group](https://iextrading.com/developer/docs/)