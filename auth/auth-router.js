'use strict';
const config = require('../config');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const express = require('express');
const router = express.Router();

const createAuthToken = function(user) {
    return jwt.sign({ user }, config.JWT_SECRET, {
        subject: user.username,
        expiresIn: config.JWT_EXPIRY,
        algorithm: 'HS256'
    })
}

const localAuth = passport.authenticate('local', { session: false });

router.use(express.json());

router.post('/login', localAuth, (req, res) => {
    // req.body has keys username and password
    const authToken = createAuthToken(req.user.serialize());
    return res.json({ token: authToken, username: req.body.username });
})

module.exports = { router };