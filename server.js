'use strict';
require('dotenv').config();
const { PORT, DATABASE_URL } = require('./config');
const { localStrategy, jwtStrategy } = require('./auth/strategies');
const { router: authRouter } = require('./auth/auth-router');
const { router: usersRouter } = require('./users/users-router');
const { router: portfolioRouter } = require('./portfolio/portfolio-router');

const express = require('express');
const passport = require('passport');
const morgan = require('morgan');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const app = express();

app.use(morgan('common'));
app.use(express.static('public'));
app.use(express.json());

app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
	res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
	
	if (req.method === 'OPTIONS') {
		return res.send(204);
	}
	next();
});

passport.use(localStrategy);
passport.use(jwtStrategy);

const jwtAuth = passport.authenticate('jwt', { session: false });

app.use('/api/users', usersRouter);
app.use('/api/auth', authRouter);
app.use('/api/portfolio', jwtAuth, portfolioRouter);

app.use('*', (req, res) => {
    return res.status(404).json({ message: 'Not found' });
})

let server;
function runServer(db) {
    return new Promise((resolve, reject) => {
        mongoose.connect(db, { useNewUrlParser: true }, (err) => {
            if (err) {
                return reject(err);
            }

            server = app.listen(PORT, () => {
                console.log(`App is listening on port ${PORT}`);
                resolve();
            })
            .on('error', (err) => {
                mongoose.disconnect();
                reject(err);
            })
        })
    })
}

function closeServer() {
    return mongoose
        .disconnect()
        .then(() => {
            return new Promise((resolve, reject) => {
                console.log('Closing server');
                server.close((err) => {
                    if (err) {
                        return reject(err);
                    }

                    resolve();
                })
            })
        })
}

if (require.main === module) {
    runServer(DATABASE_URL).catch((err) => {
        console.error(err)
    })
}

module.exports = { app, runServer, closeServer };