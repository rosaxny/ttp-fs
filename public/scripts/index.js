'use strict';
if (window.localStorage.token && window.localStorage.username) {
    let { username, token } = window.localStorage
    loggedIn({ username, token })
} else {
    document.querySelector('.registration-form-error').innerHTML = '';
    document.querySelector('.sign-in-form-error').innerHTML = '';
    document.querySelector('.password-error').innerHTML = '';
    document.querySelector('.registration-form').addEventListener('submit', (e) => {
        e.preventDefault()
        let username = document.getElementById('username').value
        let name = document.getElementById('name').value
        let password = document.getElementById('password').value
        let passwordConfirmation = document.getElementById('password-confirmation').value
        if (password !== passwordConfirmation) {
            document.querySelector('.password-error').innerHTML = 'Passwords must match';
        }
        registerUser({ username, name, password })
    })
    
    document.querySelector('.sign-in-form').addEventListener('submit', (e) => {
        e.preventDefault()
        let username = document.getElementById('signin-username').value
        let password = document.getElementById('signin-password').value
        return fetch(`/api/auth/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username,
                password
            })
        })
        .then((res) => normalizeRes(res))
        .then((_res) => _res.json())
        .then((user) => {
            loggedIn(user);
        })
        .catch((err) => {
            console.error(err)
            document.querySelector('.sign-in-form-error').innerHTML = `<p>${err.message}</p>`
        })
    })
}

function registerUser(data) {
    return fetch(`/api/users/register`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    })
    .then((res) => normalizeRes(res))
    .then((_res) => _res.json())
    .then((user) => {
        loggedIn(user);
    })
    .catch((err) => {
        console.error(err)
        document.querySelector('.registration-form-error').innerHTML = `<p>${err.message}</p>`
    })
}

function normalizeRes(res) {
    if (!res.ok) {
        if ( res.headers.has('content-type') && res.headers.get('content-type').startsWith('application/json') ) {
            // JSON error returned by API, so decode it
            return res.json()
                .then((err) => Promise.reject(err));
        }

        // less informative error returned by express
        return Promise.reject({
            code: res.status,
            message: res.statusText
        });
    }
    return res;
}

function getPortfolio() {
    return fetch(`/api/portfolio`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${window.localStorage.token}`
        }
    })
    .then((res) => normalizeRes(res))
    .then((_res) => _res.json())
    .then((portfolio) => {
        window.localStorage.removeItem('balance')
        window.localStorage.setItem('balance', portfolio.balance)
        const balance = JSON.parse(window.localStorage.balance)
        document.querySelector('.container').innerHTML = `
            <div class="margin-bottom-15">
                <h1>Portfolio</h1>
                <div class="portfolio"></div>
            </div>
            <div>
                <div class="purchase-form-error"></div>
                <form class="purchase-form">
                    <h2>Cash: $${balance}</h2>
                    <div>
                        <div>
                            <label for="ticker-symbol">Ticker Symbol:</label>
                            <br>
                            <input id="ticker-symbol" type="text" required>
                        </div>
                        <div>
                            <label for="stock-shares">Quantity:</label>
                            <br>
                            <input id="stock-shares" type="number" step="1" min="1" required>
                        </div>
                    </div>
                    <button>Buy</button>
                </form>
            </div>
        `
        document.querySelector('.purchase-form').addEventListener('submit', (e) => {
            e.preventDefault()
            let stock = document.getElementById('ticker-symbol').value
            let shares = document.getElementById('stock-shares').value
            let data = {
                stock,
                shares,
                username: window.localStorage.username,
                balance: parseFloat(window.localStorage.balance).toFixed(2)
            }
            purchaseStock(data)
        })
        let portfolioValue = portfolio.balance;
        // if (portfolio.countStockShares) {
            let renderStockInfo = Object.keys(portfolio.countStockShares).map((el) => {
                const { shares, currentPrice } = portfolio.countStockShares[el]
                let tickerSymbol = el.toUpperCase()
                let currentValue = shares * currentPrice;
                portfolioValue += currentValue;
                let priceChange = (currentPrice - portfolio.countStockShares[el].avg).toFixed(2);
                let str = `
                    <div>
                        <p class="portfolio-stock-info">${tickerSymbol}  ${shares} shares - $${currentValue.toFixed(2)}
                `
                // user story 6: change color if current price changes
                if (priceChange < 0) {
                    str += ` <span class="decrease">${priceChange}</span></p></div>`
                } else if (priceChange > 0) {
                    str += ` <span class="increase">+${priceChange}</span></p></div>`
                } else {
                    str += `</p></div>`
                }
                return str;
            }).join(' ')
        // }
        let renderHTML = `<h2>($${portfolioValue.toFixed(2)})<h2>` + renderStockInfo;
        document.querySelector('.portfolio').innerHTML = renderHTML;
        
    })
    .catch((err) => {
        console.error(err)
    })
}

function loggedIn(data) {
    window.localStorage.setItem('username', data.username);
    window.localStorage.setItem('token', data.token);
    document.querySelector('nav').classList.remove('hidden');
    getPortfolio();
}

function purchaseStock(data) {
    return fetch(`/api/portfolio/transaction`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${window.localStorage.token}`
            },
            body: JSON.stringify(data)
        })
        .then((res) => normalizeRes(res))
        .then((_res) => _res.json())
        .then((portfolio) => {
            document.querySelector('.purchase-form-error').innerHTML = '';
            document.querySelector('.purchase-form-error').innerHTML = `<p>${portfolio.message}</p>`;
            getPortfolio();
        })
        .catch((err) => {
            console.error(err)
            document.querySelector('.purchase-form-error').innerHTML = `<p>${err.error}</p>`
        })
}